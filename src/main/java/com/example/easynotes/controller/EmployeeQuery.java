package com.example.easynotes.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Employee;
import com.example.easynotes.repository.EmployeeRepository;

@RestController
@RequestMapping("/query")
public class EmployeeQuery {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	//Menampilkan semua Employee
	@GetMapping("/employees")
    public List<Employee> showList() {
        return employeeRepository.findAllActiveUsers();
    }
	
	//Mencari Employee dengan ID
	@GetMapping("/employees/{id}")
	 public HashMap<String, Object> showByID(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		List<Employee> employee = employeeRepository.searchByID(id);
		
		process.put("Messages", "Search By ID Success");
		process.put("Data", employee);
		return process;
    }

	//Menambahkan Employee
	@PostMapping("/employees/add")
    public HashMap<String, Object> add(@Valid @RequestBody Employee employee) {
        HashMap<String, Object> addProcess = new HashMap<String, Object>();
        
        employeeRepository.addEmployee(employee.getAddress(), employee.getAge(), employee.getEmployeeName(), employee.getJobdesc(), employee.getSalary());
		
		addProcess.put("Message", "Success");
		return addProcess;
    }
	
	//Mengedit Notes
	@PutMapping("/employees/{id}")
    public HashMap<String, Object> updateEmployees(@PathVariable(value = "id") Long employeeId, @Valid @RequestBody Employee employee) {
		HashMap<String, Object> process = new HashMap<String, Object>();
        Employee test = employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));        
  
        employee.setEmployeeId(test.getEmployeeId());
        if (employee.getAddress() == null) {
        	employee.setAddress(test.getAddress());
        }
        if (employee.getEmployeeName() == null) {
        	employee.setEmployeeName(test.getEmployeeName());
        }
        if (employee.getAge() == 0) {
        	employee.setAge(test.getAge());
        }
        if (employee.getJobdesc() == null) {
        	employee.setJobdesc(test.getJobdesc());
        }
        if (employee.getSalary() == 0) {
        	employee.setSalary(test.getSalary());
        }
        
        employeeRepository.updateEmployee(employee.getAddress(), employee.getAge(), employee.getEmployeeName(), employee.getJobdesc(), employee.getSalary(), employee.getEmployeeId());
        process.put("Message", "Success Updated");
        process.put("Data", employee);
        return process;
    }
	
	//Menghapus Employee		
	@DeleteMapping("/employees/{id}")
    public HashMap<String, Object> deleteEmployee(@PathVariable(value = "id") Long noteId) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		Employee employee = employeeRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Employee", "id", noteId));
		
		process.put("Messages", "Success Deleting Data Notes");
		process.put("Delete data :", employee);
		employeeRepository.deleteEmployee(noteId);
        return process;
    }
}
