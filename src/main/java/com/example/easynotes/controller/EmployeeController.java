package com.example.easynotes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Employee;
import com.example.easynotes.repository.EmployeeRepository;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    // Get All Notes
    @GetMapping("/employees")
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }
    
    @GetMapping("/software-engineers")
    public HashMap<String, Object> getSoftwareEngineer() {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Employee> cups = new ArrayList<Employee>();
    	int total = 0;
    	for(Employee test: employeeRepository.findAll()) {
    		if(test.getJobdesc().equalsIgnoreCase("Software Engineer")) {
    			cups.add(test);
    			total++;
    		}
    	}
    	newList.put("Message", "Load Software Engginner Success");
    	newList.put("Total", total);
    	newList.put("Data", cups);
        return newList;       
    }
    
    public int averageAge() {
    	int total = 0;
    	int totalAge = 0;
    	for(Employee test: employeeRepository.findAll()) {
    		totalAge += test.getAge();
    		total++;
    	}
    	int averageAge = totalAge / total;
    	return averageAge;
    }
    
    @GetMapping("/employees/ageAboveAverage")
    public HashMap<String, Object> getAgeEmployeeAboveAVG() {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Employee> cups = new ArrayList<Employee>();
    	int total = 0;
    	int averageAge = averageAge();
    	for(Employee test: employeeRepository.findAll()) {
    		if(test.getAge() > averageAge) {
    			cups.add(test);
    			total++;
    		}
    	}
    	newList.put("Message", "Read Average Success");
    	newList.put("Average Age", averageAge);
    	newList.put("Total", total);
    	newList.put("Data", cups);
        return newList;       
    }
    
    public double getHighestSalary() {
    	double highestValue = 0;
    	for(Employee emp : employeeRepository.findAll()) {
    		double tempValue = emp.getSalary();
    		if(tempValue > highestValue) {
    			highestValue = tempValue;
    		}
    	}
    	return highestValue;
    }
    
    @GetMapping("/employees/highestSalary")
    public HashMap<String, Object> getEmployeeHighestSalary() {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Employee> cups = new ArrayList<Employee>();
    	int total = 0;
    	double highestSalary = getHighestSalary();
    	for(Employee test: employeeRepository.findAll()) {
    		if(test.getSalary() == highestSalary) {
    			cups.add(test);
    			total++;
    		}
    	}
    	newList.put("Message", "Read Highest Salary Success");
    	newList.put("Highest Salary", highestSalary);
    	newList.put("Total", total);
    	newList.put("Data", cups);
        return newList;       
    }
    
    @GetMapping("/employees/containsName")
    public HashMap<String, Object> getEmployeeDra(@Valid @RequestParam(name = "name") String words) {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Employee> cups = new ArrayList<Employee>();
    	int total = 0;
    	for(Employee test: employeeRepository.findAll()) {
    		if(test.getEmployeeName().contains(words)) {
    			cups.add(test);
    			total++;
    		}
    	}
    	newList.put("Message", "Read Name Contains Success");
    	newList.put("Total", total);
    	newList.put("Data", cups);
        return newList;       
    }
    
    // Create a new Note
    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }
    
    @PostMapping("/employees/addMore")
    public HashMap<String, Object> createMoreEmployee(@Valid @RequestBody Employee... employee) {
    	HashMap<String, Object> newList = new HashMap<String, Object>();
    	List<Employee> listEmp = new ArrayList<Employee>();
    	int total = 0;
        for(Employee test : employee) {
    		total++;
    		employeeRepository.save(test);
    		listEmp.add(test);
    	}
        newList.put("Message", "Add More Employees Success");
        newList.put("Total", total);
    	newList.put("Data", listEmp);
    	return newList;
    }
    
    // Get a Single Note
    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable(value = "id") Long employeeId) {
        return employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
    }
    
    // Update a Note
    @PutMapping("/employees/{id}")
    public Employee updateEmployee(@PathVariable(value = "id") Long employeeId, @Valid @RequestBody Employee employeeDetails) {

        Employee employee = employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));

        employee.setEmployeeName(employeeDetails.getEmployeeName());
        employee.setAddress(employeeDetails.getAddress());
        employee.setAge(employeeDetails.getAge());
        employee.setJobdesc(employeeDetails.getJobdesc());
        employee.setSalary(employeeDetails.getSalary());

        Employee updatedEmployee = employeeRepository.save(employee);
        return updatedEmployee;
    }
    
    // Delete a Note
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId).orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));

        employeeRepository.delete(employee);

        return ResponseEntity.ok().build();
    }
}