package com.example.easynotes.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Writer;
import com.example.easynotes.repository.WriterRepository;

@RestController
@RequestMapping("/api")
public class WriterController {

    @Autowired
    WriterRepository writerRepository;

    // Get All Notes
    @GetMapping("/writers")
    public List<Writer> getAllWriters() {
        return writerRepository.findAll();
    }
    
    // Create a new Note
    @PostMapping("/writers")
    public Writer createWriter(@Valid @RequestBody Writer writer) {
        return writerRepository.save(writer);
    }
    
    // Get a Single Note
    @GetMapping("/writers/{id}")
    public Writer getWriterById(@PathVariable(value = "id") Long writerId) {
        return writerRepository.findById(writerId).orElseThrow(() -> new ResourceNotFoundException("Writer", "id", writerId));
    }
    
    // Update a Note
    @PutMapping("/writers/{id}")
    public Writer updateWriter(@PathVariable(value = "id") Long writerId, @Valid @RequestBody Writer writerDetails) {

        Writer writer = writerRepository.findById(writerId).orElseThrow(() -> new ResourceNotFoundException("Writer", "id", writerId));

        writer.setF_name(writerDetails.getF_name());
        writer.setL_name(writerDetails.getL_name());
        writer.setAge(writerDetails.getAge());

        Writer updatedWriter = writerRepository.save(writer);
        return updatedWriter;
    }
    
    // Delete a Note
    @DeleteMapping("/writers/{id}")
    public ResponseEntity<?> deleteWriter(@PathVariable(value = "id") Long writerId) {
        Writer writer = writerRepository.findById(writerId).orElseThrow(() -> new ResourceNotFoundException("Writer", "id", writerId));

        writerRepository.delete(writer);

        return ResponseEntity.ok().build();
    }
}