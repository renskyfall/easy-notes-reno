package com.example.easynotes.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Book;
import com.example.easynotes.repository.NoteRepository;

@RestController
@RequestMapping("/query")
public class NoteQuery {
	
	@Autowired
	NoteRepository noteRepository;
	
	//Mencari semua Notes
	@GetMapping("/notes")
    public List<Book> showList() {
        return noteRepository.findAllActiveUsers();
    }
	
	//Mencari notes dengan ID
	@GetMapping("/notes/{id}")
	 public HashMap<String, Object> showByID(@PathVariable(value = "id") Long noteId) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		List<Book> note = noteRepository.searchByID(noteId);
		
		process.put("Messages", "Search By ID Success");
		process.put("Data", note);
		return process;
    }
	
	//Menambahkan notes 
	@PostMapping("/notes/add")
    public HashMap<String, Object> addNotes(@Valid @RequestBody Book note) {
        HashMap<String, Object> addProcess = new HashMap<String, Object>();
        
        noteRepository.addNotes(note.getTitle(), note.getContent());
		
		addProcess.put("Message", "Success");
		return addProcess;
    }
	
	//Mengedit Notes
	@PutMapping("/notes/{id}")
    public HashMap<String, Object> updateNotes(@PathVariable(value = "id") Long noteId, @Valid @RequestBody(required=false) Book noteDetails) {
		HashMap<String, Object> process = new HashMap<String, Object>();
        Book note = noteRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
        
        if(noteDetails.getContent() == null && noteDetails.getTitle() == null) {
        	noteRepository.updateNotes(note.getTitle(), note.getContent(), note.getId());
        }else if(noteDetails.getContent() == null && noteDetails.getTitle() != null) {
        	noteRepository.updateNotes(noteDetails.getTitle(), note.getContent(), note.getId());
        }else if(noteDetails.getContent() != null && noteDetails.getTitle() == null) {
        	noteRepository.updateNotes(note.getTitle(), noteDetails.getContent(), note.getId());
        }else {
        	noteRepository.updateNotes(noteDetails.getTitle(), noteDetails.getContent(), note.getId());
        }

        process.put("Message", "Success Updated");
        return process;
    }
	
	//Menghapus Notes
	@DeleteMapping("/notes/{id}")
    public HashMap<String, Object> deleteNote(@PathVariable(value = "id") Long noteId) {
		HashMap<String, Object> process = new HashMap<String, Object>();
		Book note = noteRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
		
		process.put("Messages", "Success Deleting Data Notes");
		process.put("Delete data :", note);
		noteRepository.deleteNotes(noteId);
        return process;
    }
}
