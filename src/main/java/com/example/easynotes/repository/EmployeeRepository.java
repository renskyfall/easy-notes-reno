package com.example.easynotes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.easynotes.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	@Query(value = "select * from Employees", nativeQuery = true)
	public List<Employee> findAllActiveUsers();
	
	@Query(value = "select * from Employees where employee_id = :id", nativeQuery = true)
	public List<Employee> searchByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Employees (address, age, employee_name, jobdesc, salary) VALUES (:address, :age, :employee_name, :jobdesc, :salary)", nativeQuery = true)
	@Transactional
	public void addEmployee(@Param("address") String address, @Param("age") int age, @Param("employee_name") String name, @Param("jobdesc") String jobdesc, @Param("salary") double salary);
	
	@Modifying
	@Query(value = "UPDATE Employees SET address = :address, age = :age, employee_name = :name, jobdesc = :jobdesc, salary = :salary WHERE employee_id = :id", nativeQuery = true)
	@Transactional
	public void updateEmployee(@Param("address") String address, @Param("age") int age, @Param("name") String name, @Param("jobdesc") String jobdesc, @Param("salary") double salary, @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Employees WHERE id = :answer", nativeQuery = true)
	@Transactional
	public void deleteEmployee(@Param("answer") Long id);
}