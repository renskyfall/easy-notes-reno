package com.example.easynotes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.easynotes.model.Book;


@Repository
public interface NoteRepository extends JpaRepository<Book, Long> {
	
	@Query(value = "select * from Notes", nativeQuery = true)
	public List<Book> findAllActiveUsers();
	
	@Query(value = "select * from Notes where id = :id", nativeQuery = true)
	public List<Book> searchByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Notes (title, content, created_at, updated_at) VALUES (:title, :content, NOW(), NOW())", nativeQuery = true)
	@Transactional
	public void addNotes(@Param("title") String title, @Param("content") String content);
	
	@Modifying
	@Query(value = "UPDATE Notes SET title = :title, content = :content, updated_at = NOW() WHERE id = :id", nativeQuery = true)
	@Transactional
	public void updateNotes(@Param("title") String title, @Param("content") String content, @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Notes WHERE id = :answer", nativeQuery = true)
	@Transactional
	public void deleteNotes(@Param("answer") Long id);
}